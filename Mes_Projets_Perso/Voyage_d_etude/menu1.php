<!DOCTYPE html>
<html>
<body>
<h2>Click the Button.</h2>
<p>A script tag with a src attribute is created and placed in the document.</p>
<p>The PHP file returns a call to a function with the JSON object as a parameter.</p>

<button onclick="clickButton()">Click me!</button>

<p id="demo"></p>

<script>
    function clickButton() {
        var s = document.createElement("script");
        s.src = "jsoon.php";
        document.body.appendChild(s);
    }

    function myFunc(myObj) {
        document.getElementById("demo").innerHTML = myObj.name;
    }
</script>

</body>
</html>
<h2>Make a table based on the value of a drop down menu.</h2>

<select id="myselect" onchange="change_myselect(this.value)">
    <option value="">Choose an option:</option>
    <option value="employee">Customers</option>
    <option value="employees">Products</option>
    <option value="suppliers">Suppliers</option>
</select>


<h2>Get data as JSON from a PHP file, and convert it into a JavaScript array.</h2>
<p id="demo"></p>
<p id="demo1"></p>

<script>
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myObj = JSON.parse(this.responseText);
            var myObj2 = JSON.parse(this.responseText);
            document.getElementById("demo").innerHTML = myObj[0].[0].om;
            document.getElementById("demo1").innerHTML = myObj2[2];
        }
    };
    xmlhttp.open("GET", "dataTest.json", true);
    xmlhttp.send();

    function change_myselect(sel) {
        var obj, dbParam, xmlhttp, myObj, x, txt = "";
        obj = { table: sel, limit: 20 };
        dbParam = JSON.stringify(obj);
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                myObj = JSON.parse(this.responseText);
                txt += "<table border='1'>"
                for (x in myObj) {
                    txt += "<tr><td>" + myObj[x].name + "</td></tr>";
                }
                txt += "</table>"
                document.getElementById("demo1").innerHTML = txt;
            }
        };
        xmlhttp.open("POST", "dataTest.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("x=" + dbParam);
    }

</script>

</body>
</html>

<?php
/**
 * Created by PhpStorm.
 * User: Xavier
 * Date: 25.01.18
 * Time: 13:51
 */

// This function simply returns some hardcoded data
function getData()
{
    return json_decode('[{"Firstname":"Joe","Lastname":"Dalton","Hobby":"Escape room"},{"Firstname":"Jack","Lastname":"Dalton","Hobby":"Creuser des tunnels"},{"Firstname":"William","Lastname":"Dalton","Hobby":"L\'\u00e9sot\u00e9risme"},{"Firstname":"Averell","Lastname":"Dalton","Hobby":"Gober des oeufs"},{"Firstname":"Lucky","Lastname":"Luke","Hobby":"Chopper des m\u00e9chants"}]');
}

// ============== Load or create data ================

$dataDirectory = "";
$dataFileName = "menu.json";

if (file_exists("$dataFileName")) // the file already exists -> load it
{
    $data = json_decode(file_get_contents("$dataFileName"));
}
else
{
    if (!file_exists($dataDirectory)) // Check if data directory exists
    {
        mkdir($dataDirectory); // if not create it
    }
    $data = getData(); // Initialize data with fixed values
}

// ============== Process commands from GET parameters ================

extract($_GET); // possible variables created: $init, $create, $update, $delete, $index, $firstname, $lastname, $hobby

// ============== Save data ================

//file_put_contents("$dataFileName", json_encode($data));

// ============== Display data ================



// ============== Display data ================
$data1=json_decode(file_get_contents("menu.json"));
print_r($data1[0].[0]);



foreach ($data as $usr)
{
    echo "<br><table><tr><th>Nom</th><th>Prénom</th></tr>";
    echo "<tr><td>".$usr->Nom."</td><td>".$usr->Prenom."</td></tr></table><br>";
    echo "<table><tr><th>Date</th><th>Animal</th><th>X</th><th>Y</th><th>Nom d'image</th></tr>";

    foreach ($usr->Observations as $obs) // on prend le sous-tableau des observations (1 image par observation)
    {
        echo "<tr><td>".$obs->Date."</td><td>".$obs->Animal."</td><td>".$obs->X."</td><td>".$obs->Y."</td><td>"
            ."<a href='images/".$obs->Nom."'><img src='images/".$obs->Nom."' height='120'></a> </td></tr>";

    }
    echo "</table>";

}
?>



