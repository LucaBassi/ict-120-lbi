<?php
/**
 * Created by PhpStorm.
 * User: luca.bassi
 * Date: 20.03.2019
 * Time: 15:47
 */

function newUser($userName, $userPassword, $userEmail){
    $dataDirectory = 'data';
    $fileName = 'users.json';

    $newUser = ['username'=>$userName, 'password'=>$userPassword, 'email'=>$userEmail];

    if (file_exists("$dataDirectory/$fileName")) // le fichier existe
    {
        $data = json_decode(file_get_contents("$dataDirectory/$fileName"),true); //transforme les valeurs du fichier json en tableau associatif
    }
    else
    {
        if (!file_exists($dataDirectory)) // si le dossier existe pas
        {
            mkdir($dataDirectory); //on le crée
        }
    }

    $data[] = $newUser;

    file_put_contents("$dataDirectory/$fileName", json_encode($data));


}


function getUser($login, $password){
    $dataDirectory = 'data';
    $fileName = 'users.json';

    if (file_exists("$dataDirectory/$fileName")) // le fichier existe
    {
        $data = json_decode(file_get_contents("$dataDirectory/$fileName"),true);//transforme les valeurs du fichier json en tableau associatif
    }

    foreach ($data as $user){
        if($login == $user['email'] && $password == $user['password']) { // si le login et le mot de passe corréspondent retourne les infos de l'utilisateur
            return $user;
        }
    }

    $_SESSION["login"] = $login;
    $_GET["action"] = "home";

    return false;
}
require "Vue/home.php";