<?php
if (isset($_GET)) {
    extract($_GET);
}




$url = 'https://www.prevision-meteo.ch/services/json/nyon';

if (isset($ListeVille)) {
    $url = $ListeVille;
}


$jsonfile = file_get_contents($url);
$json = json_decode($jsonfile);
ob_start();
?>

    <!doctype html>

    <html>
    <head>
        <meta charset="utf-8">

        <title>Stationboard Example - Transport API</title>

        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" />
        <link rel="stylesheet" href="media/css/layout.css" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
        <script>

            $(function () {

                var station = '8501120';

                function refresh() {
                    if (station) {
                        $.get('http://transport.opendata.ch/v1/stationboard', {id: station, limit: 5}, function(data) {
                            $('#stationboard tbody').empty();
                            $(data.stationboard).each(function () {
                                var prognosis, departure, delay, line = '<tr><td>';
                                departure = moment(this.stop.departure);
                                if (this.stop.prognosis.departure) {
                                    prognosis = moment(this.stop.prognosis.departure);
                                    delay = (prognosis.valueOf() - departure.valueOf()) / 60000;
                                    line += departure.format('HH:mm') + ' <strong>+' + delay + ' min</strong>';
                                } else {
                                    line += departure.format('HH:mm');
                                }
                                line += '</td><td>' + this.name + '</td><td>' + this.to + '</td></tr>';
                                $('#stationboard tbody').append(line);
                            });
                        }, 'json');
                    }
                }

                $('#station').autocomplete({
                    source: function (request, response) {
                        $.get('../v1/locations', {query: request.term, type: 'station'}, function(data) {
                            response($.map(data.stations, function(station) {
                                return {
                                    label: station.name,
                                    station: station
                                }
                            }));
                        }, 'json');
                    },
                    select: function (event, ui) {
                        station = ui.item.station.id;
                        refresh();
                    }
                });

                setInterval(refresh, 30000);
                refresh();
            });

        </script>


    </head>
    <body>





<section class="inner">
        <table  class="alt">
            <thead>
            <tr>
                <th></th> <th></th> <th></th>
                <th ><h2><?php echo $json->city_info->name; ?></h2></th>
                <th style="text-align: right" >  <p> Temps actuel     :<img src="<?php echo $json->current_condition->icon_big;?>"></th>
                <th></th> <th></th> <th></th>
            </tr>
            </thead>
        </table>

        <div class="table-wrapper">
            <table class="alt">
                <thead>
                <tr >
                    <th><h3>Journee</h3></th>
                    <th><h3>General</h3></th>
                    <th><h3>Matin</h3></th>
                    <th><h3>Midi</h3></th>
                    <th><h3>Soirée</h3></th>
                    <th><h3>Min/Max °C</h3></th>
                    <th style="width:10%"><h3>Pluie mm</h3></th>


                </tr>
                </thead>
                <tbody>
                <tr>
                    <td ><?php echo $json->fcst_day_0->day_long; ?></td>
                    <td><?php echo $json->fcst_day_0->condition;?></td>
                    <td><img src="<?php echo $json->fcst_day_0->hourly_data->{'9H00'}->ICON;?>"></td>
                    <td><img src="<?php echo $json->fcst_day_0->hourly_data->{'14H00'}->ICON;?>"</td>
                    <td><img src="<?php echo $json->fcst_day_0->hourly_data->{'20H00'}->ICON;?>"</td>
                    <td><?php echo $json->fcst_day_0->tmax ," - ", $json->fcst_day_0->tmin;?></td>
                    <td ><?php echo $json->fcst_day_0->hourly_data->{'0H00'}->APCPsfc; ?></td>
                </tr>
<!--                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>-->
                <tr>
                    <td><?php echo $json->fcst_day_1->day_long; ?></td>
                    <td><?php echo $json->fcst_day_1->condition;?></td>
                    <td><img src="<?php echo $json->fcst_day_1->hourly_data->{'9H00'}->ICON;?>"></td>
                    <td><img src="<?php echo $json->fcst_day_1->hourly_data->{'14H00'}->ICON;?>"</td>
                    <td><img src="<?php echo $json->fcst_day_1->hourly_data->{'20H00'}->ICON;?>"</td>
                    <td><?php echo $json->fcst_day_1->tmax ," - ", $json->fcst_day_0->tmin;?></td>
                    <td><?php echo $json->fcst_day_1->hourly_data->{'0H00'}->APCPsfc; ?></td>
                </tr>
<!--                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>-->
                <tr>
                    <td><?php echo $json->fcst_day_2->day_long; ?></td>
                    <td><?php echo $json->fcst_day_2->condition;?></td>
                    <td><img src="<?php echo $json->fcst_day_2->hourly_data->{'9H00'}->ICON;?>"></td>
                    <td><img src="<?php echo $json->fcst_day_2->hourly_data->{'14H00'}->ICON;?>"</td>
                    <td><img src="<?php echo $json->fcst_day_2->hourly_data->{'20H00'}->ICON;?>"</td>
                    <td><?php echo $json->fcst_day_2->tmax ," - ", $json->fcst_day_0->tmin;?></td>
                    <td><?php echo $json->fcst_day_2->hourly_data->{'0H00'}->APCPsfc; ?></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="2"></td>
                    <td></td>
                </tr>
                </tfoot>
            </table>
        </div>
    <FORM class= method="post" action="transport.php" class="liste" NAME="ChoixVille"">
        <SELECT   style="margin-left:3%;width:25%; " id="ListeVille" NAME="ListeVille" onchange =>
            <OPTION VALUE='https://www.prevision-meteo.ch/services/json/nyon'>Nyon
            <OPTION VALUE='https://www.prevision-meteo.ch/services/json/geneve'>Geneve
            <OPTION VALUE='https://www.prevision-meteo.ch/services/json/paris'>Paris
        </SELECT>
        <br><br>
        <input  style="margin-left:3%" type="submit" value="Send Request" class="primary" />
    </FORM>


<div>
    <section>
    <form  method="get" action="transport.php" >
            <table>
                <tr>
                    <td>
                        <button style="max-width: 60%; height: auto" type="button" class="button small" id="showImage" onclick="showImageCffAller()">Cff -> Aller</button>
                        <button style="max-width: 60%; height: auto" type="button" class="button small" id="showImage" onclick="showImageCffAller()">Cff -> Aller</button>
                    </td>
                </tr>
                <tbody>
                <tr>
                    <td>
                        <img style="max-width: 60% ;height: auto; position: center" id="cff_aller" src="">
                    </td>

                </tr>
                </tbody>
            </table>
            <table>
                <thead>
                <tr>
                    <td>
                        <button style="max-width: 60% ;height: auto"  type="button" class="button small" id="showImage" onclick="showImageCffRetour()">Cff <- Retour</button>

                        <button style="max-width: 60% ;height: auto"  type="button" class="button small" id="showImage" onclick="showImageCffRetour()">Cff <- Retour</button>
                    </td>
                    <td>
                        <h4>Resume du voyage</h4>
                    </td>

                </tr>
                </thead>
                <tr>

                </tr>
                <tbody>
                <tr>
                    <td>
                        <img style="max-width: 60% ;height: auto; position: center" id="cff_retour" src="">
                    </td>
                    <td><textarea disabled> disabled>Depart ---- Ste-Croix ---- 8h06 ---- Voie 1
                                    sadcjdjldjcdc
                                ashe</textarea></td>
                </tr>
                </tbody>
            </table>
        </form>
</div>
    </section>
<section class="inner">
<div>

<p><a href="../data/mydata.json" target="_blank">Take a look at -> fichier json brut - Personnes</a>
<p><a href="../data/CFF/transport_data.json" target="_blank">Take a look at -> fichier json brut - Transports</a></p>

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../media/css/layout.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css">


</div>
    </body>




<script>
    <?php





/*
    document.getElementById('ListeVille').onchange = function() {
        window.location = "transport.php" ;
    };
*/





/*
document.addEventListener('showImage').onchange(showImageCffAller())
*/
?>


    function showImageCffAller() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                //met le contenu du ficjier json dans un objet javascript
                var myObj = JSON.parse(this.responseText);
                    document.getElementById("cff_aller").src=(Object(myObj.transports[0].cff_aller));
                }
        };
        //chemin du fichier json
        xmlhttp.open("GET", "data/CFF/transport_data.json", true);
        xmlhttp.send();
    }





    function showImageCffRetour() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                //met le contenu du ficjier json dans un objet javascript
                var myObj = JSON.parse(this.responseText);

                    document.getElementById("cff_retour").src=(Object(myObj.transports[0].cff_aller));

                }
        };
        //chemin du fichier json
        xmlhttp.open("GET", "data/CFF/transport_data.json", true);
        xmlhttp.send();
    }
//window.onload = showImage() ;
</script>








<?php
$contenu = ob_get_clean();
require "gabarit.php";

