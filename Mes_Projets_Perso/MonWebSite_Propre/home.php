<?php
/**
 *Bassi Luca
 */


/*
    include('functions.php');

    if (!isLoggedIn()) {
        $_SESSION['msg'] = "You must log in first";
        header('location: login.php');
    }*/

ob_start();
?>

<?php include "connections.php";?>
<?php include "stationboard.php";?>

<!DOCTYPE html>

<head>
    <meta charset="utf-8">

    <title>Stationboard Example - Transport API</title>

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="media/css/layout.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>



<div>

<div class="container">
    <div class="col-sm-8 col-sm-offset-2">

        <header>
            <h1><a href="">Transport</a></h1>
            <p>Swiss public transport API</p>
        </header>

        <article>
            <p class="pull-right">
                <a class="btn btn-link" href="https://github.com/OpendataCH/Transport/blob/master/web/examples/home.php" target="_blank">
                    <span class="glyphicon glyphicon-new-window"></span>
                    Source Code
                </a>
            </p>
            <h3>Stationboard Example</h3>
            <div class="ui-widget station">
                <input class="form-control" id="station" value="Lausanne" placeholder="Station" />
            </div>

        <br><br><br><br>
        <footer class="footer">
            <hr>
            <p>Powered by <a href="http://opendata.ch/">Opendata.ch</a></p>
        </footer>

    </div>
</div>

    <div class="content">



<div id="content"> <p>Vous etes dans la section Aceuil</div>


        <div>
                <img style="vertical-align: center;display: flex; max-width:70%;padding:5%;position: center;margin: auto" src="image/cpnv.png" ></span>
    </div>
        <!-- One -->
        <section id="one">
            <div class="inner">

                <img style="vertical-align: center;display: flex; width:100%;position: center;margin: auto" src="images/pic11.jpg" alt="2" />
            </div>
        </section>
    <br>
</div>
<div class="inner">
                <p>Donec eget ex magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque venenatis dolor imperdiet dolor mattis sagittis. Praesent rutrum sem diam, vitae egestas enim auctor sit amet. Pellentesque leo mauris, consectetur id ipsum sit amet, fergiat. Pellentesque in mi eu massa lacinia malesuada et a elit. Donec urna ex, lacinia in purus ac, pretium pulvinar mauris. Curabitur sapien risus, commodo eget turpis at, elementum convallis elit. Pellentesque enim turpis, hendrerit.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis dapibus rutrum facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam tristique libero eu nibh porttitor fermentum. Nullam venenatis erat id vehicula viverra. Nunc ultrices eros ut ultricies condimentum. Mauris risus lacus, blandit sit amet venenatis non, bibendum vitae dolor. Nunc lorem mauris, fringilla in aliquam at, euismod in lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non lorem sit amet elit placerat maximus. Pellentesque aliquam maximus risus, vel sed vehicula.</p>
                <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque venenatis dolor imperdiet dolor mattis sagittis. Praesent rutrum sem diam, vitae egestas enim auctor sit amet. Pellentesque leo mauris, consectetur id ipsum sit amet, fersapien risus, commodo eget turpis at, elementum convallis elit. Pellentesque enim turpis, hendrerit tristique lorem ipsum dolor.</p>
<!-- Placez ici des informations complémentaires -->
</div>


<?php
// $contenu = ob_get_clean();
require "gabarit.php";
;?>