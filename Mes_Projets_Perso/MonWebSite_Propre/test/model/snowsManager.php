<?php
/**
 * Created by PhpStorm.
 * Author: Samuel-Souka.MEYER
 * Date: 13.02.2019
 * Time: 14:05
 */

function getSnows()
{
    $snowsQuery = "SELECT code, brand, model, snowLength, dailyPrice, qtyAvailable, photo FROM snows";

    require "model/dbConnector.php";
    $snowsResults = executeQuery($snowsQuery);
    return $snowsResults;
}

?>