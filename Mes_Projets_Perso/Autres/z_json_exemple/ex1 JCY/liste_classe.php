<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>JSON CRUD</title>
</head>
<body>
<?php

// ============== Load data ================

$dataFileName = "classe.json";

if (file_exists("$dataFileName")) // the file already exists -> load it
{
    $data = json_decode(file_get_contents($dataFileName));
}
else
{
	echo "fichier données non trouvées";
}

// ============== Display data ================

echo "classe: ".$data->classe."<BR>";
echo "Maitre de classe: ";
echo $data->maitreclasse->acronyme." ";
echo $data->maitreclasse->nom." ";
echo $data->maitreclasse->prénom." "."<BR>";

foreach ($data->eleves as $el){
	echo $el->nom." ".$el->prénom." ".$el->annee."<BR>";	
}

?>


