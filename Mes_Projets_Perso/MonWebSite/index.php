<?php

session_start();

require 'controler.php';
//require 'Vue/css/style.css';

if (isset($_GET['action']))
{
    switch ($_GET['action'])
    {
        case 'details' :
            details();
            break;

        case 'nouveau' :
            nouveau();
            break;

        default :
            home();
            break;

        case 'home' :
            home();
            break;
    }
}
else
{
    home();
}
